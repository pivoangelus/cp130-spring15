//
//  HWObject.h
//  hmwk04
//
//  Created by Justin Andros on 5/14/15.
//  Copyright (c) 2015 Justin Andros. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HWObject : NSObject

@property (nonatomic, strong) NSMutableDictionary *objectDictionary;

- (instancetype)initWithData;

@end
