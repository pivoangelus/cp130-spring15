//
//  MasterViewController.h
//  hmwk04
//
//  Created by Justin Andros on 5/13/15.
//  Copyright (c) 2015 Justin Andros. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HWObjectDelegate.h"

@class DetailViewController;

@interface MasterViewController : UITableViewController
<
    HWLogoutDelegate
>

@property (strong, nonatomic) DetailViewController *detailViewController;

@end

