 //
//  HWObject.m
//  hmwk04
//
//  Created by Justin Andros on 5/14/15.
//  Copyright (c) 2015 Justin Andros. All rights reserved.
//

#import "HWObject.h"
#import "HWConstants.h"

@implementation HWObject

- (instancetype)initWithData;
{
    self = [super self];
    
    if (self)
    {
        NSString *dateStr = [NSString stringWithFormat:@"%@", [[NSDate date] description]];
        
        _objectDictionary = [NSMutableDictionary dictionaryWithDictionary:@{ kHWObjectDataItem : dateStr,
                                                                             kHWObjectStudent  : STUDENTID,
                                                                             }];
    }
    
    return self;
}

@end
