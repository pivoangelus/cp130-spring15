//
//  HWObjectDelegate.h
//  hmwk04
//
//  Created by Justin Andros on 5/14/15.
//  Copyright (c) 2015 Justin Andros. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "HWObject.h"

@protocol HWLogoutDelegate <NSObject>

@required
- (void)logout;

@end

@protocol HWObjectDelegate <NSObject>

@required
@property (nonatomic, weak) id<HWLogoutDelegate> logoutDelegate;
@property (nonatomic, strong) HWObject *detailItem;

@end
