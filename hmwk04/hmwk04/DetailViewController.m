//
//  DetailViewController.m
//  hmwk04
//
//  Created by Justin Andros on 5/13/15.
//  Copyright (c) 2015 Justin Andros. All rights reserved.
//

#import "DetailViewController.h"
#import "AppDelegate.h"
#import "HWConstants.h"

@interface DetailViewController ()
{
    MSClient *client;
}
@property (nonatomic, strong) UIBarButtonItem *logoutButton;
@property (weak, nonatomic) IBOutlet UILabel *dataitemOutputLabel;
@property (weak, nonatomic) IBOutlet UILabel *studentOutputLabel;
@property (weak, nonatomic) IBOutlet UILabel *idOutputLabel;

@end

@implementation DetailViewController

#pragma mark - Managing the detail item

- (void)setDetailItem:(id)newDetailItem
{
    if (_detailItem != newDetailItem)
    {
        _detailItem = newDetailItem;
            
        [self configureView];
    }
}

- (void)configureView
{
    // toggle between data and none
    if (self.detailItem)
    {
        self.dataitemOutputLabel.text = [self.detailItem.objectDictionary valueForKey:kHWObjectDataItem];
        self.studentOutputLabel.text = [self.detailItem.objectDictionary valueForKey:kHWObjectStudent];
        self.idOutputLabel.text = [self.detailItem.objectDictionary valueForKey:kHWObjectID];
    }
    else
    {
        self.dataitemOutputLabel.text = @"";
        self.studentOutputLabel.text = @"";
        self.idOutputLabel.text = @"";
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // add a logout button into the bar
    self.logoutButton = [[UIBarButtonItem alloc] initWithTitle:@"Logout" style:UIBarButtonItemStylePlain target:self action:@selector(detailLogout)];
    self.navigationItem.rightBarButtonItem = self.logoutButton;
    
    [self configureView];
}

- (void)detailLogout
{
    // delegate call to logout in MasterViewController
    self.detailItem = nil;
    [self configureView];
    [self.logoutDelegate logout];
}

@end
