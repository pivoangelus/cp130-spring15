//
//  MasterViewController.m
//  hmwk04
//
//  Created by Justin Andros on 5/13/15.
//  Copyright (c) 2015 Justin Andros. All rights reserved.
//

#import "MasterViewController.h"
#import "DetailViewController.h"
#import "AppDelegate.h"
#import "HWObject.h"
#import "HWConstants.h"

@interface MasterViewController ()
{
    MSClient *client;
    MSTable *table;
}
@property (nonatomic, strong) NSMutableArray  *objects;
@property (nonatomic, strong) UIBarButtonItem *loginButton;
@property (nonatomic, strong) UIBarButtonItem *spaceButton;
@property (nonatomic, strong) UIBarButtonItem *logoutButton;
@property (nonatomic, strong) UIBarButtonItem *addButton;

@end

@implementation MasterViewController

- (void)awakeFromNib
{
    [super awakeFromNib];
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad)
    {
        self.clearsSelectionOnViewWillAppear = NO;
        self.preferredContentSize = CGSizeMake(320.0, 600.0);
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    client = [(AppDelegate *) [[UIApplication sharedApplication] delegate] client];
    
    self.navigationItem.leftBarButtonItem = self.editButtonItem;
    
    // create buttons and link some to actions
    self.loginButton = [[UIBarButtonItem alloc] initWithTitle:@"Login" style:UIBarButtonItemStylePlain target:self action:@selector(login)];
    self.spaceButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    self.logoutButton = [[UIBarButtonItem alloc] initWithTitle:@"Logout" style:UIBarButtonItemStylePlain target:self action:@selector(logout)];
    self.addButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(insertNewObject:)];

    // connect the detailViewController
    self.detailViewController = (DetailViewController *)[[self.splitViewController.viewControllers lastObject] topViewController];
    
    [self updateView];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    // connect the azure table and load the data, otherwise just update the bar
    if (client.currentUser)
    {
        table = [client tableWithName:AZURETABLE];
        [self reloadData];
    }
    else
        [self updateView];
}

- (void)login
{
    [client loginWithProvider:@"google" controller:self animated:YES completion:nil];
}

- (void)logout
{
    [client logout];
    
    // delete cookies made
    for (NSHTTPCookie *value in [NSHTTPCookieStorage sharedHTTPCookieStorage].cookies)
        [[NSHTTPCookieStorage sharedHTTPCookieStorage] deleteCookie:value];

    // reset data made.
    client.currentUser = nil;
    table = nil;
    self.objects = [NSMutableArray array];
    
    // if in DetailViewController collapse to the root
    if (self.splitViewController.collapsed)
        [self.splitViewController.viewControllers[0] popToRootViewControllerAnimated:YES];
    
    [self updateView];
}

- (void)reloadData
{
    // grab only data we want
    NSString *predicateString = [NSString stringWithFormat:@"%@ == '%@'", kHWObjectStudent, STUDENTID];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:predicateString];
    
    [table readWithPredicate:predicate completion:^(MSQueryResult *result, NSError *error) {
        self.objects = [NSMutableArray array];
        
        NSLog(@"%lu items retrieved from Azure", (unsigned long)result.items.count);
        
        // place all the data into a local array
        for (NSDictionary *item in result.items)
        {
            HWObject *objectItem = [[HWObject alloc] init];
            
            objectItem.objectDictionary = [NSMutableDictionary dictionaryWithDictionary:item];
            [self.objects addObject:objectItem];
        }
        [self updateView];
    }];
}

- (void)updateView
{
    // make sure the array bar is nil. don't want extra buttons loading
    self.navigationItem.rightBarButtonItems = nil;
    
    if (!client.currentUser)
        // if the user isn't logged in don't give them the option to add
        self.navigationItem.rightBarButtonItem = self.loginButton;
    else
        // place the add button with a logout button with spacing. logout button here may be a very bad idea UI design wise.
        // just felt odd having to go into a detail to logout
        self.navigationItem.rightBarButtonItems = @[ self.addButton, self.spaceButton, self.logoutButton, self.spaceButton ];
    
    [self.tableView reloadData];
}

- (void)insertNewObject:(id)sender
{
    HWObject *objectItem = [[HWObject alloc] initWithData];
    
    [table insert:objectItem.objectDictionary completion:^(NSDictionary *item, NSError *error) {
        if (error)
            NSLog(@"<MasterViewController insertNewObject:> %@", error);
        else
        {
            NSLog(@"Item inserted, id: %@", [item objectForKey:kHWObjectID]);
            [objectItem.objectDictionary setValue:[item objectForKey:kHWObjectID] forKey:kHWObjectID];
            
            [self.objects addObject:objectItem];
            
            [self reloadData];
        }
    }];
}

#pragma mark - Segues

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"showDetail"])
    {
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        
        HWObject *objectItem = (HWObject *)self.objects[indexPath.row];
        
        self.detailViewController = (DetailViewController *)[[segue destinationViewController] topViewController];
        
        [self.detailViewController setDetailItem:objectItem];
        self.detailViewController.logoutDelegate = self;
        
        self.detailViewController.navigationItem.leftBarButtonItem = self.splitViewController.displayModeButtonItem;
        self.detailViewController.navigationItem.leftItemsSupplementBackButton = YES;
    }
}

#pragma mark - Table View

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.objects.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];

    HWObject *objectItem = self.objects[indexPath.row];
    cell.textLabel.text = [objectItem.objectDictionary valueForKey:kHWObjectDataItem];
    return cell;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete)
    {
        HWObject *objectItemToDelete = self.objects[indexPath.row];
        
        [table delete:objectItemToDelete.objectDictionary completion:^(id itemId, NSError *error) {
            if (error)
                NSLog(@"<MasterViewController tableView:commitEditingStyle:forRowAtIndexPath:> %@", error);
            else
            {
                [self.objects removeObjectAtIndex:indexPath.row];
                [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
                NSLog(@"Item deleted, id: %@", itemId);
                [self reloadData];
            }
        }];
    }
    else if (editingStyle == UITableViewCellEditingStyleInsert)
    {
    }
}

@end
