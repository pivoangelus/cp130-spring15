//
//  HWConstants.m
//  hmwk04
//
//  Created by Justin Andros on 5/14/15.
//  Copyright (c) 2015 Justin Andros. All rights reserved.
//

#import "HWConstants.h"

// dictionary keys
NSString *kHWObjectDataItem = @"dataitem";
NSString *kHWObjectStudent  = @"student";
NSString *kHWObjectID       = @"id";

// constants
NSString *STUDENTID         = @"jandros";
NSString *AZURETABLE        = @"homework";
NSString *AZUREURL          = @"https://cp130-uw-test.azure-mobile.net/";
