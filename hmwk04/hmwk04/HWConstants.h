//
//  HWConstants.h
//  hmwk04
//
//  Created by Justin Andros on 5/14/15.
//  Copyright (c) 2015 Justin Andros. All rights reserved.
//

#import <Foundation/Foundation.h>

extern NSString *kHWObjectDataItem;
extern NSString *kHWObjectStudent;
extern NSString *kHWObjectID;
extern NSString *STUDENTID;
extern NSString *AZUREURL;
extern NSString *AZURETABLE;