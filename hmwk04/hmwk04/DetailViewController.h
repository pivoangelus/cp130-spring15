//
//  DetailViewController.h
//  hmwk04
//
//  Created by Justin Andros on 5/13/15.
//  Copyright (c) 2015 Justin Andros. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HWObject.h"
#import "HWObjectDelegate.h"

@interface DetailViewController : UIViewController
<
    HWObjectDelegate
>

@property (nonatomic, weak) id<HWLogoutDelegate> logoutDelegate;
@property (strong, nonatomic) HWObject *detailItem;

@end

