//
//  DetailViewController.h
//  hmwk02
//
//  Created by Justin Andros on 4/30/15.
//  Copyright (c) 2015 Justin Andros. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GameDelegate.h"
#import "GameEntry.h"

@interface DetailViewController : UIViewController
<
    GameDelegate
>

@property (nonatomic, weak) GameEntry *game;
@property (nonatomic, copy) void (^updateGamesBlock)(void);

@end

