//
//  EditGameViewController.h
//  hmwk02
//
//  Created by Justin Andros on 4/30/15.
//  Copyright (c) 2015 Justin Andros. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GameDelegate.h"
#import "GameEntry.h"

@interface EditGameViewController : UIViewController
<
    GameDelegate
>

@property (nonatomic, weak) id<GameDelegate> gameDelegate;

@end
