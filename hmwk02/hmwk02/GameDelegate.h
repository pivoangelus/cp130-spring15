//
//  GameDelegate.h
//  hmwk02
//
//  Created by Justin Andros on 4/30/15.
//  Copyright (c) 2015 Justin Andros. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GameEntry.h"

@protocol GameDelegate <NSObject>

@required

@optional
@property (nonatomic, weak) id<GameDelegate> gameDelegate;
@property (nonatomic, weak) GameEntry *game;
@property (nonatomic, copy) void (^updateGamesBlock)(void);

- (void)updateGame:(NSString *)gameName platform:(NSString *)platform rating:(NSUInteger)rating;
- (void)updateGameCanceled;
- (GameEntry *)fetchGame;

@end