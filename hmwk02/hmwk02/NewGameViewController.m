//
//  NewGameViewController.m
//  hmwk02
//
//  Created by Justin Andros on 4/30/15.
//  Copyright (c) 2015 Justin Andros. All rights reserved.
//

#import "NewGameViewController.h"

@interface NewGameViewController ()

@property (weak, nonatomic) IBOutlet UITextField *gameNameTextField;
@property (weak, nonatomic) IBOutlet UITextField *platformTextField;
@property (weak, nonatomic) IBOutlet UILabel *ratingLabel;
@property (weak, nonatomic) IBOutlet UISlider *ratingSlider;

- (IBAction)updateLabelValue:(UISlider *)sender;
- (IBAction)cancelButtonTapped:(id)sender;
- (IBAction)doneButtonTapped:(id)sender;

@end

@implementation NewGameViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.gameNameTextField.text = self.game.gameName;
    self.platformTextField.text = self.game.platform;
    self.ratingLabel.text = [NSString stringWithFormat:@"Score: %lu", (unsigned long)self.game.rating];
    self.ratingSlider.value = self.game.rating;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (IBAction)updateLabelValue:(UISlider *)sender
{
    self.ratingLabel.text = [NSString stringWithFormat:@"Score: %lu", (unsigned long)self.ratingSlider.value];
}

- (IBAction)cancelButtonTapped:(id)sender
{
    [self.presentingViewController dismissViewControllerAnimated:YES completion:self.cancelAddGameBlock];
}

- (IBAction)doneButtonTapped:(id)sender
{
    self.game.gameName = self.gameNameTextField.text;
    self.game.platform = self.platformTextField.text;
    self.game.rating = self.ratingSlider.value;
    
    [self.presentingViewController dismissViewControllerAnimated:YES completion:self.saveAddedGameBlock];
}

@end
