//
//  GameEntry.m
//  hmwk02
//
//  Created by Justin Andros on 4/30/15.
//  Copyright (c) 2015 Justin Andros. All rights reserved.
//

#import "GameEntry.h"
#import "defaults.h"

@implementation GameEntry

- (instancetype)initWithDefaults
{
    self = [super init];
    
    if (self)
    {
        _gameName = [[NSUserDefaults standardUserDefaults] stringForKey:kGameNameDefault];
        _platform = [[NSUserDefaults standardUserDefaults] stringForKey:kPlatformDefault];
        _rating = [[NSUserDefaults standardUserDefaults] integerForKey:kRatingDefault];
    }
    
    return self;
}

@end
