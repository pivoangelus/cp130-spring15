//
//  DetailViewController.m
//  hmwk02
//
//  Created by Justin Andros on 4/30/15.
//  Copyright (c) 2015 Justin Andros. All rights reserved.
//

#import "DetailViewController.h"

@interface DetailViewController ()

@property (weak, nonatomic) IBOutlet UILabel *gameNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *platformLabel;
@property (weak, nonatomic) IBOutlet UILabel *ratingLabel;

@property (weak, nonatomic) IBOutlet UILabel *gameNameOutputLabel;
@property (weak, nonatomic) IBOutlet UILabel *platformOutputLabel;
@property (weak, nonatomic) IBOutlet UILabel *ratingOutputLabel;
@property (weak, nonatomic) IBOutlet UILabel *missingGameMessageOutputLabel;

@property UIViewController <GameDelegate> *gameController;

@property (weak, nonatomic) IBOutlet UIBarButtonItem *composeBarButtonItem;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *editBarButtonItem;

- (void)isHidden:(BOOL)isHidden;
- (void)updateView;

#define kEditDetailPopover @"editDetailPopover"
#define kEditDetailModal   @"editDetailModal"
#define missingGameMsg     @"Add and select a game!"

@end

@implementation DetailViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self updateView];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self updateView];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:kEditDetailPopover] ||
        [[segue identifier] isEqualToString:kEditDetailModal])
    {
        UIViewController *vc = [segue destinationViewController];
        
        self.gameController = (UIViewController <GameDelegate> *)vc;
        self.gameController.gameDelegate = self;
    }
}

- (void)isHidden:(BOOL)isHidden;
{
    [self.gameNameLabel setHidden:isHidden];
    [self.platformLabel setHidden:isHidden];
    [self.ratingLabel setHidden:isHidden];
    
    [self.gameNameOutputLabel setHidden:isHidden];
    [self.platformOutputLabel setHidden:isHidden];
    [self.ratingOutputLabel setHidden:isHidden];
    
    [self.missingGameMessageOutputLabel setHidden:!isHidden];

    [self.composeBarButtonItem setEnabled:!isHidden];
    [self.editBarButtonItem setEnabled:!isHidden];
}

- (void)updateView;
{
    if (!self.game)
    {
        self.missingGameMessageOutputLabel.text = missingGameMsg;
        [self isHidden:YES];
    }
    else
    {
        self.gameNameOutputLabel.text = self.game.gameName;
        self.platformOutputLabel.text = self.game.platform;
        self.ratingOutputLabel.text = [NSString stringWithFormat:@"%lu", (unsigned long)self.game.rating];
        
        [self isHidden:NO];
    } 
}

#pragma mark - <GameEditDetailDelegate>

- (void)updateGame:(NSString *)gameName platform:(NSString *)platform rating:(NSUInteger)rating;
{
    self.game.gameName = gameName;
    self.game.platform = platform;
    self.game.rating = rating;
    
    [self updateView];
    
    [self.gameController dismissViewControllerAnimated:YES completion:self.updateGamesBlock];
}

- (void)updateGameCanceled;
{
    [self.gameController dismissViewControllerAnimated:YES completion:nil];
}

- (GameEntry *)fetchGame;
{
    return self.game;
}

@end
