//
//  EditGameViewController.m
//  hmwk02
//
//  Created by Justin Andros on 4/30/15.
//  Copyright (c) 2015 Justin Andros. All rights reserved.
//

#import "EditGameViewController.h"

@interface EditGameViewController ()

@property (weak, nonatomic) IBOutlet UITextField *gameNameTextField;
@property (weak, nonatomic) IBOutlet UITextField *platformTextField;
@property (weak, nonatomic) IBOutlet UILabel *ratingLabel;
@property (weak, nonatomic) IBOutlet UISlider *ratingSlider;

- (IBAction)updateLabelValue:(UISlider *)sender;
- (IBAction)doneButtonTapped:(id)sender;
- (IBAction)cancelButtonTapped:(id)sender;

@end

@implementation EditGameViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    GameEntry *game = [self.gameDelegate fetchGame];
    
    self.gameNameTextField.text = game.gameName;
    self.platformTextField.text = game.platform;
    self.ratingLabel.text = [NSString stringWithFormat:@"Score: %lu", (unsigned long)game.rating];
    self.ratingSlider.value = game.rating;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (IBAction)updateLabelValue:(UISlider *)sender
{
    self.ratingLabel.text = [NSString stringWithFormat:@"Score: %lu", (unsigned long)self.ratingSlider.value];
}

- (IBAction)doneButtonTapped:(id)sender
{
    [self.gameDelegate updateGame:self.gameNameTextField.text
                         platform:self.platformTextField.text
                           rating:self.ratingSlider.value];
}

- (IBAction)cancelButtonTapped:(id)sender
{
    [self.gameDelegate updateGameCanceled];
}

@end
