//
//  MasterViewController.m
//  hmwk02
//
//  Created by Justin Andros on 4/30/15.
//  Copyright (c) 2015 Justin Andros. All rights reserved.
//
//  game ratings

#import "DetailViewController.h"
#import "GameEntry.h"
#import "MasterViewController.h"
#import "NewGameViewController.h"

@interface MasterViewController ()
<
    UITableViewDataSource,
    UITableViewDelegate
>

@property (nonatomic, strong) NSMutableArray *games;

#define kAddGameViewSegue    @"addGame"
#define kShowDetailViewSegue @"showDetail"

@end

@implementation MasterViewController

- (void)awakeFromNib
{
    [super awakeFromNib];
    self.clearsSelectionOnViewWillAppear = NO;
    self.preferredContentSize = CGSizeMake(320.0, 600.0);
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationItem.leftBarButtonItem = self.editButtonItem;
    self.games = [NSMutableArray array];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - Segues

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
    
    if ([[segue identifier] isEqualToString:kShowDetailViewSegue])
    {
        DetailViewController *controller = (DetailViewController *)[[segue destinationViewController] topViewController];
        
        controller.navigationItem.leftBarButtonItem = self.splitViewController.displayModeButtonItem;
        controller.navigationItem.leftItemsSupplementBackButton = YES;
        
        if (indexPath)
            controller.game = self.games[indexPath.row];
        
        controller.updateGamesBlock =
        ^{
            [self.tableView reloadData];
            
            if (self.games.count && indexPath)
                [self.tableView selectRowAtIndexPath:indexPath animated:NO scrollPosition:UITableViewScrollPositionNone];
        };
    }
    
    if ([[segue identifier] isEqualToString:kAddGameViewSegue])
    {
        UIViewController *vc = [[segue destinationViewController] topViewController];
        
        NewGameViewController *agvc = (NewGameViewController *)vc;
        
        GameEntry *game = [[GameEntry alloc] initWithDefaults];
        [self.games addObject:game];
        agvc.game = game;
        
        agvc.saveAddedGameBlock =
        ^{
            [self.tableView reloadData];
        };
        
        agvc.cancelAddGameBlock =
        ^{
            [self.games removeObject:game];
        };
    }
}

#pragma mark - Table View

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.games.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];

    GameEntry *game = self.games[indexPath.row];
    cell.textLabel.text = [NSString stringWithFormat:@"%@", game.gameName];
    return cell;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete)
    {
        [self.games removeObjectAtIndex:indexPath.row];
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
        [self performSegueWithIdentifier:kShowDetailViewSegue sender:self];
    }
    else if (editingStyle == UITableViewCellEditingStyleInsert)
    {
    }
}

@end
