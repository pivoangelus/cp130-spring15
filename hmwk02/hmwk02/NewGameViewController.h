//
//  NewGameViewController.h
//  hmwk02
//
//  Created by Justin Andros on 4/30/15.
//  Copyright (c) 2015 Justin Andros. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GameEntry.h"

@interface NewGameViewController : UIViewController

@property (nonatomic, weak) GameEntry *game;
@property (nonatomic, copy) void (^cancelAddGameBlock)(void);
@property (nonatomic, copy) void (^saveAddedGameBlock)(void);

@end
