//
//  AppDelegate.m
//  hmwk02
//
//  Created by Justin Andros on 4/30/15.
//  Copyright (c) 2015 Justin Andros. All rights reserved.
//

#import "AppDelegate.h"
#import "DetailViewController.h"
#import "defaults.h"

@interface AppDelegate ()
<
    UISplitViewControllerDelegate
>

@end

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    [[NSUserDefaults standardUserDefaults] registerDefaults:
    @{
        kGameNameDefault : gameNameDefault,
        kPlatformDefault : platformDefault,
        kRatingDefault   : @ratingDefault
    }];
    
    [[NSNotificationCenter defaultCenter] addObserverForName:NSUserDefaultsDidChangeNotification
                                                      object:[NSUserDefaults standardUserDefaults]
                                                       queue:[NSOperationQueue mainQueue]
                                                  usingBlock:^(NSNotification *note) {
                                                      [[NSUserDefaults standardUserDefaults] synchronize];
                                                  }];
    
    UISplitViewController *splitViewController = (UISplitViewController *)self.window.rootViewController;
    UINavigationController *navigationController = [splitViewController.viewControllers lastObject];
    navigationController.topViewController.navigationItem.leftBarButtonItem = splitViewController.displayModeButtonItem;
    splitViewController.delegate = self;
    return YES;
}

- (void)dealloc
{
    // make sure to remove observer. appdelegate will take care of this but it is nice to have.
    [[NSUserDefaults standardUserDefaults] synchronize];
    [[NSNotificationCenter defaultCenter] removeObserver:[NSUserDefaults standardUserDefaults]];
}

#pragma mark - Split view

- (BOOL)splitViewController:(UISplitViewController *)splitViewController collapseSecondaryViewController:(UIViewController *)secondaryViewController ontoPrimaryViewController:(UIViewController *)primaryViewController
{
    if ([secondaryViewController isKindOfClass:[UINavigationController class]] && [[(UINavigationController *)secondaryViewController topViewController] isKindOfClass:[DetailViewController class]] && ([(DetailViewController *)[(UINavigationController *)secondaryViewController topViewController] game] == nil))
    {
        // Return YES to indicate that we have handled the collapse by doing nothing; the secondary controller will be discarded.
        return YES;
    }
    else
    {
        return NO;
    }
}

@end
