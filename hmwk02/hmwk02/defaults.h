//
//  defaults.h
//  hmwk02
//
//  Created by Justin Andros on 5/1/15.
//  Copyright (c) 2015 Justin Andros. All rights reserved.
//

#define kGameNameDefault @"kGameNameDefault"
#define kPlatformDefault @"kPlatformDefault"
#define kRatingDefault   @"kRatingDefault"

#define gameNameDefault  @"Game Title"
#define platformDefault  @"PC"
#define ratingDefault    80