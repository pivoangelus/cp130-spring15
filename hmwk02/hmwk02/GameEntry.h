//
//  GameEntry.h
//  hmwk02
//
//  Created by Justin Andros on 4/30/15.
//  Copyright (c) 2015 Justin Andros. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GameEntry : NSObject

@property (nonatomic, copy) NSString *gameName;
@property (nonatomic, copy) NSString *platform;
@property (nonatomic) NSUInteger rating;

- (instancetype)initWithDefaults;

@end
