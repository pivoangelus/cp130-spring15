//
//  NewItemViewController.h
//  hmwk05
//
//  Created by Justin Andros on 5/26/15.
//  Copyright (c) 2015 Justin Andros. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CoreDataDelegate.h"

@interface NewItemViewController : UIViewController
<
    CreateItemDelegate
>

@property (nonatomic, strong) Item *createItem;
@property (nonatomic, weak) id<UtilityDelegate> utilityDelegate;

@end
