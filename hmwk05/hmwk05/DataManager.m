//
//  DataManager.m
//  hmwk05
//
//  Created by Justin Andros on 5/26/15.
//  Copyright (c) 2015 Justin Andros. All rights reserved.
//

#import "DataManager.h"

@interface DataManager ()
{   // "private" arrays
    // was easier to work with the values in the db this way -- need to review CoreData fetch
    NSMutableArray *pItems;
    NSMutableArray *pPrices;
    // "private" CoreData
    NSManagedObjectModel *model;
    NSManagedObjectContext *context;
}

@end

@implementation DataManager

- (instancetype)init
{
    self = [super init];

    if (!self)
        return self;
    
    model = [NSManagedObjectModel mergedModelFromBundles:nil];
    NSPersistentStoreCoordinator *coordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:model];
    
    NSArray *documentDirs = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *saveDir = [documentDirs firstObject];
    NSString *path = [saveDir stringByAppendingPathComponent:@"hmwk05Model.sqlite"];
    NSURL *storeURL = [NSURL fileURLWithPath:path];
    
    NSDictionary *options = @{ NSSQLitePragmasOption: @{@"journal_mode" : @" DELETE"}};
    NSError *error;
    
    if (![coordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:options error:&error])
        [NSException raise:@"Failed to open" format:@"%@", [error localizedDescription]];
    
    context = [[NSManagedObjectContext alloc] init];
    context.persistentStoreCoordinator = coordinator;
    
    // once the db is connected load any data available
    [self load];
    
    return self;
}

// create one store to be used throughout the application
+ (DataManager *)sharedStore
{
    static dispatch_once_t pred;
    static DataManager *sharedStore;
    
    dispatch_once(&pred, ^{
        sharedStore = [[self alloc] init];
    });
    
    return sharedStore;
}

- (NSArray *)items
{
    return [pItems copy];   // give the user a copy to read
}

- (NSArray *)prices
{
    return [pPrices copy];  // give the user a copy to read
}

- (BOOL)save;
{
    NSError *error = nil;
    BOOL saved = [context save:&error];
    
    if (!saved)
        NSLog(@"save unsuccessful: %@", [error localizedDescription]);
    else
        NSLog(@"save successful");
    
    return saved;
}

- (Item *)createItem;
{
    Item *item = [NSEntityDescription insertNewObjectForEntityForName:@"Item" inManagedObjectContext:context];
    [pItems addObject:item];
    return item;
}

- (Price *)createPrice;
{
    Price *price = [NSEntityDescription insertNewObjectForEntityForName:@"Price" inManagedObjectContext:context];
    [pPrices addObject:price];
    return price;
}

- (void)removeItem:(Item *)item;
{
    [context deleteObject:item];
    [pItems removeObjectIdenticalTo:item];
    [self save];
}

- (void)load
{
    if (!pItems)
    {
        NSFetchRequest *itemRequest = [[NSFetchRequest alloc] init];
        itemRequest.entity = [NSEntityDescription entityForName:@"Item" inManagedObjectContext:context];
        
        NSError *error = nil;
        NSArray *itemResult = [context executeFetchRequest:itemRequest error:&error];
        
        if (!itemResult || itemResult.count < 1)
            pItems = [NSMutableArray array];
        else
            pItems = [[NSMutableArray alloc] initWithArray:itemResult];
    }
    
    if (!pPrices)
    {
        NSFetchRequest *priceRequest = [[NSFetchRequest alloc] init];
        priceRequest.entity = [NSEntityDescription entityForName:@"Price" inManagedObjectContext:context];
        
        NSError *error = nil;
        NSArray *priceResult = [context executeFetchRequest:priceRequest error:&error];
        
        if (!priceResult || priceResult.count < 1)
            pPrices = [NSMutableArray array];
        else
            pPrices = [[NSMutableArray alloc] initWithArray:priceResult];
    }
    
    NSLog(@"hmwk05Model.sqlite loaded");
}

@end
