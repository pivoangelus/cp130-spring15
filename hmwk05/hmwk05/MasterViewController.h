//
//  MasterViewController.h
//  hmwk05
//
//  Created by Justin Andros on 5/26/15.
//  Copyright (c) 2015 Justin Andros. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CoreDataDelegate.h"

@class DetailViewController;

@interface MasterViewController : UITableViewController
<
    UITableViewDataSource,
    UITableViewDelegate,
    UtilityDelegate
>

@property (strong, nonatomic) DetailViewController *detailViewController;


@end

