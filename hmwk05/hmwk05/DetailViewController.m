//
//  DetailViewController.m
//  hmwk05
//
//  Created by Justin Andros on 5/26/15.
//  Copyright (c) 2015 Justin Andros. All rights reserved.
//

#import "DetailViewController.h"
#import "EditItemViewController.h"
#import "DataManager.h"
#import "Item.h"

@interface DetailViewController ()

@property (weak, nonatomic) IBOutlet UILabel *staticItemLabel;
@property (weak, nonatomic) IBOutlet UILabel *staticQuantityLabel;
@property (weak, nonatomic) IBOutlet UILabel *staticPriceLabel;
@property (weak, nonatomic) IBOutlet UILabel *itemNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *quantityLabel;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *editBarButtonItem;

@property EditItemViewController <EditItemDelegate> *editDetailViewController;

@end

@implementation DetailViewController

- (void)configureView
{
    // Update the user interface for the detail item.
    if (self.detailItem)
    {
        [self isHidden:NO];
        self.itemNameLabel.text = self.detailItem.name;
        self.quantityLabel.text = [NSString stringWithFormat:@"%@",self.detailItem.quantity];
        
        NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
        formatter.numberStyle = NSNumberFormatterCurrencyStyle;
        NSString *str = [formatter stringFromNumber:self.detailItem.price.cost];
        self.priceLabel.text = str;
    }
    else
    {
        [self isHidden:YES];
    }
}

- (void)isHidden:(BOOL)hide
{
    [self.staticItemLabel setHidden:hide];
    [self.staticQuantityLabel setHidden:hide];
    [self.staticPriceLabel setHidden:hide];
    [self.itemNameLabel setHidden:hide];
    [self.quantityLabel setHidden:hide];
    [self.priceLabel setHidden:hide];
    [self.editBarButtonItem setEnabled:!hide];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    [self configureView];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self configureView];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // EditItemViewController
    if ([[segue identifier] isEqualToString:@"editItemSegue"])
    {
        UIViewController *vc = [segue destinationViewController];
        
        self.editDetailViewController = (EditItemViewController <EditItemDelegate> *)vc;
        self.editDetailViewController.editItem = self.detailItem;
        self.editDetailViewController.utilityDelegate = self;
    }
}

#pragma mark - <DetailItemDelegate>

- (void)updateItemAfterDelete
{
    self.detailItem = nil;
    [self configureView];
}

- (void)setDetailItem:(Item *)newDetailItem
{
    if (_detailItem != newDetailItem)
    {
        _detailItem = newDetailItem;
        
        // Update the view.
        [self configureView];
    }
}

#pragma mark - <UtilityDelegate>

- (void)save
{
    if (!self.editDetailViewController)
        return;
    
    [[DataManager sharedStore] save];
    [self.editDetailViewController dismissViewControllerAnimated:YES completion:nil];
    [self configureView];
}

- (void)cancel
{
    if (!self.editDetailViewController)
        return;
    
    [self.editDetailViewController dismissViewControllerAnimated:YES completion:nil];
}

@end
