//
//  EditItemViewController.m
//  hmwk05
//
//  Created by Justin Andros on 5/27/15.
//  Copyright (c) 2015 Justin Andros. All rights reserved.
//

#import "EditItemViewController.h"
#import "DataManager.h"

@interface EditItemViewController ()

@property (nonatomic, weak) IBOutlet UILabel *itemNameLabel;
@property (nonatomic, weak) IBOutlet UITextField *priceTextField;
@property (nonatomic, weak) IBOutlet UISlider *quantitySlider;
@property (weak, nonatomic) IBOutlet UILabel *quantitySliderValueLabel;

- (IBAction)doneTapped:(id)sender;
- (IBAction)cancelTapped:(id)sender;

@end

@implementation EditItemViewController

-(void)viewDidLoad
{
    [super viewDidLoad];
    
    self.itemNameLabel.text = self.editItem.name;
    self.priceTextField.text = [NSString stringWithFormat:@"%@", self.editItem.price.cost];
    self.quantitySlider.value = [self.editItem.quantity integerValue];
    self.quantitySliderValueLabel.text = [NSString stringWithFormat:@"%ld", (long)[self.editItem.quantity integerValue]];
}

// make sure the user filled out the form correctly
- (BOOL)isComplete
{
    NSString *alertString = @"";
    NSError *error = nil;

    // no alpha characters
    NSRegularExpression *regexABC = [NSRegularExpression regularExpressionWithPattern:@"[a-z]+" options:NSRegularExpressionCaseInsensitive error:&error];
    // price format we are looking for
    NSRegularExpression *regexPrice = [NSRegularExpression regularExpressionWithPattern:@"^(\\s+)?\\d+[.]?\\d?\\d?(\\s+)?$" options:NSRegularExpressionCaseInsensitive error:&error];
    
    if ([regexABC numberOfMatchesInString:self.priceTextField.text options:0 range:NSMakeRange(0, [self.priceTextField.text length])])
        alertString = [alertString stringByAppendingString:@"Prices do not contain letters.\n"];
    
    if (![regexPrice numberOfMatchesInString:self.priceTextField.text options:0 range:NSMakeRange(0, [self.priceTextField.text length])])
        alertString = [alertString stringByAppendingString:@"Price format examples: 0.50, 1.25, 2 or 20"];
    
    if (![alertString isEqualToString:@""])
    {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Please Fix" message:alertString preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *alertOK = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
        
        [alert addAction:alertOK];
        [self presentViewController:alert animated:YES completion:nil];
        alertString = @"";
        return NO;
    }
    
    return YES;
}

- (IBAction)quantitySliderChanged:(id)sender
{
    self.quantitySliderValueLabel.text = [NSString stringWithFormat:@"%d", (int)self.quantitySlider.value];
}

- (IBAction)doneTapped:(id)sender
{
    self.editItem.quantity = [NSNumber numberWithInt:(int)self.quantitySlider.value];
    
    if ([self isComplete])
    {
        NSNumber *newPrice = [NSNumber numberWithDouble:[self.priceTextField.text doubleValue]];
        Price *price;
        for (Price *p in [[DataManager sharedStore] prices])
        {
            if ([newPrice isEqualToNumber:p.cost])
            {
                price = p;
                break;
            }
        }
        
        if (!price)
        {
            price = [[DataManager sharedStore] createPrice];
            price.cost = newPrice;
        }
        
        self.editItem.price = price;
        
        [self.utilityDelegate save];
    }
}

- (IBAction)cancelTapped:(id)sender
{
    [self.utilityDelegate cancel];
}

@end
