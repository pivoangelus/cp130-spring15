//
//  DetailViewController.h
//  hmwk05
//
//  Created by Justin Andros on 5/26/15.
//  Copyright (c) 2015 Justin Andros. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CoreDataDelegate.h"

@interface DetailViewController : UIViewController
<
    DetailItemDelegate,
    UtilityDelegate
>

@property (strong, nonatomic) Item *detailItem;

@end

