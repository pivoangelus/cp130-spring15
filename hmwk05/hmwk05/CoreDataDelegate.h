//
//  CoreDataDelegate.h
//  hmwk05
//
//  Created by Justin Andros on 5/27/15.
//  Copyright (c) 2015 Justin Andros. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Item.h"
#import "Price.h"

@protocol UtilityDelegate <NSObject>

@required
- (void)save;
- (void)cancel;

@optional

@end

@protocol DetailItemDelegate <NSObject>

@required
@property (nonatomic, strong) Item *detailItem;
- (void)setDetailItem:(Item *)newDetailItem;
- (void)updateItemAfterDelete;

@optional

@end

@protocol CreateItemDelegate <NSObject>

@required
@property (nonatomic, strong) Item *createItem;
@property (nonatomic, weak) id<UtilityDelegate> utilityDelegate;

@optional

@end

@protocol EditItemDelegate <NSObject>

@required
@property (nonatomic, strong) Item *editItem;
@property (nonatomic, weak) id<UtilityDelegate> utilityDelegate;

@optional

@end