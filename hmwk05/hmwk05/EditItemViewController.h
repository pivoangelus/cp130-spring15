//
//  EditItemViewController.h
//  hmwk05
//
//  Created by Justin Andros on 5/27/15.
//  Copyright (c) 2015 Justin Andros. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CoreDataDelegate.h"

@interface EditItemViewController : UIViewController
<
    EditItemDelegate
>

@property (nonatomic, strong) Item *editItem;
@property (nonatomic, weak) id<UtilityDelegate> utilityDelegate;

@end
