//
//  MasterViewController.m
//  hmwk05
//
//  Created by Justin Andros on 5/26/15.
//  Copyright (c) 2015 Justin Andros. All rights reserved.
//

#import "MasterViewController.h"
#import "DetailViewController.h"
#import "NewItemViewController.h"
#import "DataManager.h"

@interface MasterViewController ()

@property NewItemViewController <CreateItemDelegate> *createItemViewController;

@end

@implementation MasterViewController

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad)
    {
        self.clearsSelectionOnViewWillAppear = NO;
        self.preferredContentSize = CGSizeMake(320.0, 600.0);
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    self.navigationItem.leftBarButtonItem = self.editButtonItem;
    self.detailViewController = (DetailViewController *)[[self.splitViewController.viewControllers lastObject] topViewController];
}

#pragma mark - Segues

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // NewItemViewController
    if ([[segue identifier] isEqualToString:@"newItemSegue"])
    {
        UIViewController *topVC = [[segue destinationViewController] topViewController];
        NewItemViewController *vc = (NewItemViewController *)topVC;
        
        self.createItemViewController = vc;
        
        Item *newItem = [[DataManager sharedStore] createItem];
        self.createItemViewController.createItem = newItem;
        self.createItemViewController.utilityDelegate = self;
    }
    
    // DetailViewController
    if ([[segue identifier] isEqualToString:@"showDetail"])
    {
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        
        Item *selectedItem = [[DataManager sharedStore] items][indexPath.row];
        DetailViewController *detailVC = (DetailViewController *)[[segue destinationViewController] topViewController];
        // below adds the 'back' button. iPads seems to forget this for some reason.
        detailVC.navigationItem.leftBarButtonItem = self.splitViewController.displayModeButtonItem;
        detailVC.navigationItem.leftItemsSupplementBackButton = YES;

        [detailVC setDetailItem:selectedItem];
    }
}

- (void)deleteItem:(UITableView *)tableView atIndexPath:(NSIndexPath *)indexPath
{
    // AlertController window
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Delete Item?" message:@"" preferredStyle:UIAlertControllerStyleAlert];
    // OK button
    UIAlertAction *alertOK = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        // remove the item from the db
        [[DataManager sharedStore] removeItem:[[DataManager sharedStore] items][indexPath.row]];
        // remove the item from the table. iOS chooses the animation style.
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
        
        // find DetailViewController by checking to see if it conforms to DetailItemDelegate
        for (UINavigationController *nc in self.splitViewController.viewControllers)
        {
            if ([nc.visibleViewController conformsToProtocol:@protocol(DetailItemDelegate)])
            {
                // update the view with nothing, because the item is gone!
                DetailViewController *vc = (DetailViewController *)nc.visibleViewController;
                [vc updateItemAfterDelete];
            }
        }
    
        [alert dismissViewControllerAnimated:YES completion:nil];
    }];
    // Cancel button
    UIAlertAction *alertCancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        [alert dismissViewControllerAnimated:YES completion:nil];
    }];
    
    [alert addAction:alertOK];
    [alert addAction:alertCancel];
    [self presentViewController:alert animated:YES completion:nil];
}


#pragma mark - <UtilityDelegate>

- (void)save
{
    if (!self.createItemViewController)
        return;
    
    [[DataManager sharedStore] save];
    [self.createItemViewController dismissViewControllerAnimated:YES completion:nil];
    [self.tableView reloadData];
}

- (void)cancel
{
    if (!self.createItemViewController)
        return;
    
    [[DataManager sharedStore] removeItem:self.createItemViewController.createItem];
    [self.createItemViewController dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - <UITableViewDataSource>
#pragma mark - <UITableViewDelegate>

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [[DataManager sharedStore] items].count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    
    Item *item = [[DataManager sharedStore] items][indexPath.row];
    
    cell.textLabel.text = item.name;
    
    return cell;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete)
    {
        [self deleteItem:tableView atIndexPath:indexPath];
    }
    else if (editingStyle == UITableViewCellEditingStyleInsert)
    {   // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view.
    }
}

@end
