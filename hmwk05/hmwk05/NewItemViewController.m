//
//  NewItemViewController.m
//  hmwk05
//
//  Created by Justin Andros on 5/26/15.
//  Copyright (c) 2015 Justin Andros. All rights reserved.
//

#import "NewItemViewController.h"
#import "DataManager.h"

@interface NewItemViewController ()

@property (nonatomic, weak) IBOutlet UITextField *itemNameTextField;
@property (nonatomic, weak) IBOutlet UISlider *quantitySlider;
@property (nonatomic, weak) IBOutlet UILabel *quantitySliderValueLabel;
@property (nonatomic, weak) IBOutlet UITextField *priceTextField;

- (IBAction)doneTapped:(id)sender;
- (IBAction)cancelTapped:(id)sender;

@end

@implementation NewItemViewController

-(void)viewDidLoad
{
    [super viewDidLoad];
    
    self.quantitySlider.value = 0;
    self.quantitySliderValueLabel.text = [NSString stringWithFormat:@"%d", (int)self.quantitySlider.value];
}

// make sure the user filled out the form correctly
- (BOOL)isComplete
{
    NSString *alertString = @"";
    NSError *error = nil;
    // no white space
    NSRegularExpression *regexBlank = [NSRegularExpression regularExpressionWithPattern:@"^\\s*$" options:NSRegularExpressionCaseInsensitive error:&error];
    // no alpha characters
    NSRegularExpression *regexABC = [NSRegularExpression regularExpressionWithPattern:@"[a-z]+" options:NSRegularExpressionCaseInsensitive error:&error];
    // price format we are looking for
    NSRegularExpression *regexPrice = [NSRegularExpression regularExpressionWithPattern:@"^(\\s+)?\\d+[.]?\\d?\\d?(\\s+)?$" options:NSRegularExpressionCaseInsensitive error:&error];
    
    if ([regexBlank numberOfMatchesInString:self.itemNameTextField.text options:0 range:NSMakeRange(0, [self.itemNameTextField.text length])])
        alertString = [alertString stringByAppendingString:@"Item name cannot be blank.\n"];
    
    if ([regexABC numberOfMatchesInString:self.priceTextField.text options:0 range:NSMakeRange(0, [self.priceTextField.text length])])
        alertString = [alertString stringByAppendingString:@"Prices do not contain letters.\n"];
    
    if (![regexPrice numberOfMatchesInString:self.priceTextField.text options:0 range:NSMakeRange(0, [self.priceTextField.text length])])
        alertString = [alertString stringByAppendingString:@"Price format examples: 0.01, 1.25, 2 or 20"];
    
    if (![alertString isEqualToString:@""])
    {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Please Fix" message:alertString preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *alertOK = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
        
        [alert addAction:alertOK];
        [self presentViewController:alert animated:YES completion:nil];
        alertString = @"";
        return NO;
    }
    
    return YES;
}

- (IBAction)quantitySliderChanged:(id)sender
{
    self.quantitySliderValueLabel.text = [NSString stringWithFormat:@"%d", (int)self.quantitySlider.value];
}

- (IBAction)doneTapped:(id)sender
{
    if ([self isComplete])
    {
        self.createItem.name = self.itemNameTextField.text;
        self.createItem.quantity = [NSNumber numberWithInt:(int)self.quantitySlider.value];
        
        // check to see if we have the price already, otherwise create a new one
        NSNumber *newPrice = [NSNumber numberWithDouble:[self.priceTextField.text doubleValue]];
        Price *price;
        for (Price *p in [[DataManager sharedStore] prices])
        {
            if ([newPrice isEqualToNumber:p.cost])
            {
                price = p;
                break;
            }
        }
        
        if (!price)
        {
            price = [[DataManager sharedStore] createPrice];
            price.cost = newPrice;
        }
        
        self.createItem.price = price;
        
        [self.utilityDelegate save];
    }
}

- (IBAction)cancelTapped:(id)sender
{
    [self.utilityDelegate cancel];
}

@end
