//
//  Price.m
//  hmwk05
//
//  Created by Justin Andros on 5/28/15.
//  Copyright (c) 2015 Justin Andros. All rights reserved.
//

#import "Price.h"
#import "Item.h"


@implementation Price

@dynamic cost;
@dynamic items;

@end
