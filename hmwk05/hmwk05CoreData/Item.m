//
//  Item.m
//  hmwk05
//
//  Created by Justin Andros on 5/28/15.
//  Copyright (c) 2015 Justin Andros. All rights reserved.
//

#import "Item.h"
#import "Price.h"


@implementation Item

@dynamic name;
@dynamic quantity;
@dynamic price;

@end
