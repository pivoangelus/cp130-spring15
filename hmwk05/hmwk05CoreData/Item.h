//
//  Item.h
//  hmwk05
//
//  Created by Justin Andros on 5/28/15.
//  Copyright (c) 2015 Justin Andros. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Price;

@interface Item : NSManagedObject

@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSNumber * quantity;
@property (nonatomic, retain) Price *price;

@end
