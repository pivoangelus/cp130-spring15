//
//  ViewController.m
//  hmwk07
//
//  Created by Justin Andros on 6/10/15.
//  Copyright (c) 2015 Justin Andros. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

// UIImageViews to test the request
@property (weak, nonatomic) IBOutlet UIImageView *imageViewNSURLRequest;
@property (weak, nonatomic) IBOutlet UIImageView *imageViewNSURLSession;

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (NSURL *)imageURL
{   // requested image for homework
    return [NSURL URLWithString:@"http://www.washington.edu/wp-content/themes/uw-2014/assets/images/404.jpg"];
}

#pragma mark - NSURLRequest, NSURLConnection

- (IBAction)getDataUsingNSURLRequest:(id)sender
{   //  NSURLRequest, NSURLConnection, NSURLResponse
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[self imageURL]];
    [request setHTTPMethod:@"GET"];
    
    NSError *error = nil;
    NSURLResponse *urlResponse;
    NSData *data = [NSURLConnection sendSynchronousRequest:request returningResponse:&urlResponse error:&error];
    
    if (error)
    {
        NSLog(@"NSURLRequest error: %@", error);
        return;
    }
    
    UIImage *image = [UIImage imageWithData:data];
    
    if (image)
        self.imageViewNSURLRequest.image = image;
}

#pragma mark - NSURLSession

- (IBAction)getDataUsingNSURLSession:(id)sender
{   //  Foreground NSURLSession
    NSURLSessionDownloadTask *downloadTask = [[NSURLSession sharedSession] downloadTaskWithURL:[self imageURL] completionHandler:^(NSURL *location, NSURLResponse *response, NSError *error) {
        UIImage *image = [UIImage imageWithData:[NSData dataWithContentsOfURL:location]];

        if (error)
        {
            NSLog(@"NSURLSession error: %@", error);
            return;
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            if (image)
                self.imageViewNSURLSession.image = image;
        });
    }];
    [downloadTask resume];
}

@end
