//
//  ViewController.m
//  hmwk03
//
//  Created by Justin Andros on 5/5/15.
//  Copyright (c) 2015 Justin Andros. All rights reserved.
//

#import "ViewController.h"
#import "CellOperation.h"

@interface ViewController ()

@property (nonatomic, weak) IBOutlet UICollectionView *collectionView;

@property (nonatomic, strong) NSMutableArray *operations;
@property (nonatomic, strong) NSOperationQueue *queue;

@end

@implementation ViewController

#define MAXCELLS 100
#define kCollectionViewCell @"cell"

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
    self.queue = [[NSOperationQueue alloc] init];
    self.operations = [NSMutableArray array];
}

- (IBAction)start:(id)sender
{
    self.queue.suspended = NO;
    for (int i = 0; i < self.operations.count; i++)
    {
        CellOperation *oldCell = [self.operations objectAtIndex:i];
        CellOperation *newCell = [[CellOperation alloc] init];
        newCell.cell = oldCell.cell;
        newCell.cellIndex = oldCell.cellIndex;
        
        [self.queue addOperation:newCell];
        self.operations[i] = newCell;
    }
}

- (IBAction)pause:(id)sender
{
    for (int i = 0; i < self.operations.count; i++)
    {
        [[self.operations objectAtIndex:i] cancel];
    }
    self.queue.suspended = YES;
}

#pragma mark <UICollectionViewDelegate>
#pragma mark <UICollectionViewDataSource>

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section;
{
    return MAXCELLS;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath;
{
    UICollectionViewCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:kCollectionViewCell forIndexPath:indexPath];
    
    // still don't understand how some cells get filled in, while others are randomly (it seems) ignored.
    if (self.operations.count != MAXCELLS)
    {
        CellOperation *op = [[CellOperation alloc] init];
        op.cell = cell;
        op.cellIndex = indexPath.item + 1.;
        
        if (!self.queue.suspended)
        {
            [self.operations addObject:op];
            [self.queue addOperation:op];
        }
    }

    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView willDisplayCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath
{
    // trying to make sure we place the cells back into their right location, doesn't work but the objects seem to get back to their correct locations
    if (self.operations.count == MAXCELLS)
    {
        cell = [self.operations objectAtIndex:indexPath.item];
    }
}


@end
