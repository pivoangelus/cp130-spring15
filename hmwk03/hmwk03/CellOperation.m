//
//  CellOperation.m
//  hmwk03
//
//  Created by Justin Andros on 5/5/15.
//  Copyright (c) 2015 Justin Andros. All rights reserved.
//

#import "CellOperation.h"

@implementation CellOperation

- (void)main
{
    // rotating cards seemed more fun so i changed it
    UIView *card = (UIView *)[self.cell viewWithTag:4];
    UILabel *cellLabel = (UILabel *)[self.cell viewWithTag:2];
    
    [card addSubview:cellLabel];
    [self.cell.contentView addSubview:card];
    
    while (1)
    {
        if (self.isCancelled)
            return;
        
        dispatch_async(dispatch_get_main_queue(), ^{
            NSInteger suitIndex = arc4random() % 4;
            NSInteger rankIndex = arc4random() % 13;
            self.labelTitle = [NSString stringWithFormat:@"%@ %@",[self validSuits:suitIndex], [self rankStrings:rankIndex]];
            cellLabel.text = self.labelTitle;
        
            [UIView transitionWithView:self.cell.contentView
                              duration:0.2
                               options:UIViewAnimationOptionTransitionFlipFromRight
                            animations: ^{
                                dispatch_async(dispatch_get_main_queue(), ^{
                                    [card removeFromSuperview];
                                    [self.cell.contentView addSubview:card];
                                });
                            }
                            completion:NULL];
        });
        
        [NSThread sleepForTimeInterval:0.5];
    }
}

- (NSArray *)validSuits:(NSInteger)index
{
    return @[@"♥︎",@"♦︎",@"♠︎",@"♣︎"][index];
}

- (NSArray *)rankStrings:(NSInteger)index
{
    return @[@"A",@"2",@"3",@"4",@"5",@"6",@"7",@"8",@"9",@"10",@"J",@"Q",@"K"][index];
}

@end
