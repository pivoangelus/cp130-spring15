//
//  ViewController.h
//  hmwk03
//
//  Created by Justin Andros on 5/5/15.
//  Copyright (c) 2015 Justin Andros. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController
<
    UICollectionViewDelegate,
    UICollectionViewDataSource
>

@end
