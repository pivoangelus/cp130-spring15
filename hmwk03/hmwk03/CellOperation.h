//
//  CellOperation.h
//  hmwk03
//
//  Created by Justin Andros on 5/5/15.
//  Copyright (c) 2015 Justin Andros. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface CellOperation : NSOperation

@property (nonatomic, weak) UICollectionViewCell *cell;
@property (nonatomic, strong) NSString *labelTitle;
@property int cellIndex;

@end
