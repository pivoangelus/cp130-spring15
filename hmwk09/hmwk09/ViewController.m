//
//  ViewController.m
//  hmwk09
//
//  Created by Justin Andros on 6/29/15.
//  Copyright (c) 2015 Justin Andros. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@property (nonatomic, assign) BOOL canTweet;
@property (weak, nonatomic) IBOutlet UIButton *shareButton;

@end

@implementation ViewController

#pragma mark - social

// post to Twitter
- (IBAction)tweetTapped:(id)sender
{
    if (![SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter])
    {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"" message:@"Please make sure Twitter is installed on your device." preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
        [alert addAction:defaultAction];
        [self presentViewController:alert animated:YES completion:nil];
        return;
    }
    
    SLComposeViewController *tweetViewController = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
    
    [tweetViewController setInitialText:@"The voices in my head told me to..."];
    [self presentViewController:tweetViewController animated:YES completion:nil];
}

// post to your favorite installed social media
- (IBAction)shareTapped:(id)sender
{
    NSString *share = @"Share me?";
    NSArray *shareItems = @[share];
    
    UIActivityViewController *activityViewController = [[UIActivityViewController alloc] initWithActivityItems:shareItems applicationActivities:nil];
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
    {
        [self presentViewController:activityViewController animated:YES completion:nil];
    }
    else
    {
        UIPopoverController *popover = [[UIPopoverController alloc] initWithContentViewController:activityViewController];
        [popover presentPopoverFromRect:self.shareButton.frame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    }
}

@end
