//
//  MasterViewController.m
//  hmwk06
//
//  Created by Justin Andros on 6/5/15.
//  Copyright (c) 2015 Justin Andros. All rights reserved.
//

#import "MasterViewController.h"
#import "DetailViewController.h"
#import "Constants.h"
#import "ImageEntry.h"
#import "DataManager.h"

#import <MobileCoreServices/MobileCoreServices.h>
#import <AWSCore/AWSCore.h>
#import <AWSCognito/AWSCognito.h>
#import <AWSS3/AWSS3.h>

@interface MasterViewController ()

@property NSMutableArray *imageEntries;

- (IBAction)addTapped:(id)sender;

@end

@implementation MasterViewController

- (void)awakeFromNib
{
    [super awakeFromNib];
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad)
    {
        self.clearsSelectionOnViewWillAppear = NO;
        self.preferredContentSize = CGSizeMake(320.0, 600.0);
    }
    
    // connect to amazon db
    AWSCognitoCredentialsProvider *credentialsProvider = [[AWSCognitoCredentialsProvider alloc] initWithRegionType:AWSRegionUSEast1
                                                                                                    identityPoolId:CognitoIdentityPoolId];
    AWSServiceConfiguration *configuration = [[AWSServiceConfiguration alloc] initWithRegion:AWSRegionUSEast1
                                                                         credentialsProvider:credentialsProvider];
    
    [AWSCognito registerCognitoWithConfiguration:configuration forKey:cognitoKey];
    AWSCognito *syncClient = [AWSCognito CognitoForKey:cognitoKey];
    AWSCognitoDataset *dataset = [syncClient openOrCreateDataset:AWSDatasetKey];
    // continueWithExecutor:withBlock: keeps the ui in the mainThread making sure the tableView gets updated
    [[dataset synchronize] continueWithExecutor:[BFExecutor mainThreadExecutor] withBlock:^id(BFTask *task) {
        if (task.completed)
        {
            NSLog(@"dataset synchronized successful");
            [self.tableView reloadData];
        }
        else
            NSLog(@"dataset synchronized failed");
        
        return nil;
    }];
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    [self.tableView reloadData];
    self.navigationItem.leftBarButtonItem = self.editButtonItem;
    self.detailViewController = (DetailViewController *)[[self.splitViewController.viewControllers lastObject] topViewController];
}

- (IBAction)addTapped:(id)sender
{
    BOOL isPhotoLibraryAvailable = [UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary];
 
    // show the users photo library for an image to select
    if (isPhotoLibraryAvailable)
    {
        UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
        imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        imagePicker.mediaTypes = @[(NSString *)kUTTypeImage];
        imagePicker.delegate = self;
        
        imagePicker.navigationItem.leftBarButtonItem = self.splitViewController.displayModeButtonItem;
        imagePicker.navigationItem.leftItemsSupplementBackButton = YES;
        [self presentViewController:imagePicker animated:YES completion:nil];
    }
    else
        NSLog(@"can't find a library");
}


- (NSString *)imagePathWithName:(NSString *)name
{
    // return the users document folder
    NSArray *documentDirectories = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    
    NSString *documentDirectory = [documentDirectories firstObject];
    
    return [documentDirectory stringByAppendingString:[NSString stringWithFormat:@"/%@", name]];
}

- (void)deleteImageEntryIn:(UITableView *)tableView atIndexPath:(NSIndexPath *)indexPath
{
    // set objects before anything is removed
    UploadImage *uploadImage = [[DataManager sharedStore] imageEntries][indexPath.row];
    ImageEntry *imageEntry = [[DataManager sharedStore] createImageEntryFromUploadImage:uploadImage];

    // remove it from local db and table
    [[DataManager sharedStore] removeImageEntry:uploadImage];
    [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
    
    // remove it from amazon
    AWSDynamoDBObjectMapper *dynamoDBObjectMapper = [AWSDynamoDBObjectMapper defaultDynamoDBObjectMapper];
    [[dynamoDBObjectMapper remove:imageEntry] continueWithBlock:^id(BFTask *task) {
        if (task.error)
        {
            NSLog(@"Delete failed. Error: %@", task.error);
        }
        
        if (task.exception)
        {
            NSLog(@"Delete failed: Exception: %@", task.exception);
        }
        
        if (task.result)
        {
            NSLog(@"Delete successful");
        }
        
        return nil;
    }];
}

#pragma mark - <UIImagePickerControllerDelegate>

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info;
{
    // remove UIImagePicker and show selected image
    [self dismissViewControllerAnimated:YES completion:nil];
    [self.detailViewController setDetailItem:info[UIImagePickerControllerOriginalImage]];
    
#pragma mark - DynamoDB
    AWSDynamoDBObjectMapper *dynamoDBObjectMapper = [AWSDynamoDBObjectMapper defaultDynamoDBObjectMapper];
    UploadImage *uploadImage = [[DataManager sharedStore] createUploadImage];
    NSString *uniqueKey = [[NSProcessInfo processInfo] globallyUniqueString];

    uploadImage.uploadName = [NSString stringWithFormat:@"%@.JPG",uniqueKey];
    uploadImage.studentID = studentIDKey;
    uploadImage.imageID = uniqueKey;
    
    ImageEntry *imageEntry = [[DataManager sharedStore] createImageEntryFromUploadImage:uploadImage];
    
    [[dynamoDBObjectMapper save:imageEntry]
     continueWithBlock:^id(BFTask *task) {
         if (task.error) {
             NSLog(@"The request failed. Error: [%@]", task.error);
         }
         if (task.exception) {
             NSLog(@"The request failed. Exception: [%@]", task.exception);
         }
         if (task.result) {
             NSLog(@"Image entry updated");
         }
         return nil;
     }];
    
#pragma mark - S3
    AWSS3TransferManager *transferManager = [AWSS3TransferManager defaultS3TransferManager];
    AWSS3TransferManagerUploadRequest *uploadRequest = [[AWSS3TransferManagerUploadRequest alloc] init];
    
    // get the image and path
    NSString *imageName = [NSString stringWithFormat:@"%@.JPG", uniqueKey];
    NSString *path = [self imagePathWithName:imageName];
    
    NSLog(@"%@", path);
    
    // place the image somewhere accessible
    NSData *data = UIImageJPEGRepresentation(info[UIImagePickerControllerOriginalImage], 100.0);
    [data writeToFile:path atomically:YES];
    
    // upload it to S3
    uploadRequest.body = [NSURL fileURLWithPath:path];
    uploadRequest.key = uploadImage.uploadName;
    uploadRequest.bucket = S3BucketName;
    
    [transferManager upload:uploadRequest];
    
    [[DataManager sharedStore] save];
    
    [self.tableView reloadData];
}

#pragma mark - Segues

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"showDetail"])
    {
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        
        UploadImage *selectedUploadImage = [[DataManager sharedStore] imageEntries][indexPath.row];
        DetailViewController *dvc = (DetailViewController *)[[segue destinationViewController] topViewController];
        dvc.navigationItem.leftBarButtonItem = self.splitViewController.displayModeButtonItem;
        dvc.navigationItem.leftItemsSupplementBackButton = YES;
        self.detailViewController = dvc;
        
        // Download picture from S3
        AWSS3TransferManager *transferManager = [AWSS3TransferManager defaultS3TransferManager];
        
        // Construct the NSURL for the download location.
        NSString *downloadingFilePath = [NSTemporaryDirectory() stringByAppendingPathComponent:selectedUploadImage.uploadName];
        NSURL *downloadingFileURL = [NSURL fileURLWithPath:downloadingFilePath];
        
        // Construct the download request.
        AWSS3TransferManagerDownloadRequest *downloadRequest = [[AWSS3TransferManagerDownloadRequest alloc] init];
        
        downloadRequest.bucket = S3BucketName;
        downloadRequest.key = selectedUploadImage.uploadName;
        downloadRequest.downloadingFileURL = downloadingFileURL;
        
        // Download the file.
        [[transferManager download:downloadRequest] continueWithExecutor:[BFExecutor mainThreadExecutor] withBlock:^id(BFTask *task) {
            if (task.error){
                if ([task.error.domain isEqualToString:AWSS3TransferManagerErrorDomain]) {
                    switch (task.error.code) {
                        case AWSS3TransferManagerErrorCancelled:
                        case AWSS3TransferManagerErrorPaused:
                            break;
                            
                        default:
                            NSLog(@"Download Error: %@", task.error);
                            break;
                    }
                } else {
                    // Unknown error.
                    NSLog(@"Download Error: %@", task.error);
                }
            }
            
            if (task.result) {
                //File downloaded successfully.
                UIImage *image = [UIImage imageWithContentsOfFile:downloadingFilePath];
                [dvc setDetailItem:image];
            }
            return nil;
        }];
    }
}

#pragma mark - <UITableViewDataSource>
#pragma mark - <UITableViewDelegate>

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [[DataManager sharedStore] imageEntries].count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];

    UploadImage *image = [[DataManager sharedStore] imageEntries][indexPath.row];
    cell.textLabel.text = image.uploadName;
    return cell;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete)
    {
        [self deleteImageEntryIn:tableView atIndexPath:indexPath];
    }
    else if (editingStyle == UITableViewCellEditingStyleInsert)
    {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view.
    }
}

@end
