//
//  DetailViewController.h
//  hmwk06
//
//  Created by Justin Andros on 6/5/15.
//  Copyright (c) 2015 Justin Andros. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ImageEntry.h"

@interface DetailViewController : UIViewController

@property (strong, nonatomic) UIImage *detailItem;

@end

