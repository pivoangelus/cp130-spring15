//
//  ImageEntry.h
//  hmwk06
//
//  Created by Justin Andros on 6/5/15.
//  Copyright (c) 2015 Justin Andros. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AWSDynamoDB/AWSDynamoDB.h>
#import "AWSDynamoDBObjectMapper.h"

@interface ImageEntry : AWSDynamoDBObjectModel <AWSDynamoDBModeling>

@property (nonatomic, strong) NSString *ID;
@property (nonatomic, strong) NSString *studentID;
@property (nonatomic, strong) NSString *uploadName;

@end
