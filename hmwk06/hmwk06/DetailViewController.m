//
//  DetailViewController.m
//  hmwk06
//
//  Created by Justin Andros on 6/5/15.
//  Copyright (c) 2015 Justin Andros. All rights reserved.
//

#import "DetailViewController.h"
#import <MobileCoreServices/MobileCoreServices.h>

@interface DetailViewController () 

@property (weak, nonatomic) IBOutlet UIImageView *imageView;

@end

@implementation DetailViewController

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    // Do any additional setup after loading the view, typically from a nib.
    [self configureView];
}

- (void)setDetailItem:(UIImage *)newImageItem;
{
    if (_detailItem != newImageItem)
    {
        _detailItem = newImageItem;
            
        // Update the view.
        [self configureView];
    }
}

- (void)configureView
{
    if (self.detailItem)
    {
        self.imageView.image = self.detailItem;
    }
}



@end