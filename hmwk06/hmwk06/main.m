//
//  main.m
//  hmwk06
//
//  Created by Justin Andros on 6/5/15.
//  Copyright (c) 2015 Justin Andros. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
