//
//  DataManager.h
//  hmwk05
//
//  Created by Justin Andros on 5/26/15.
//  Copyright (c) 2015 Justin Andros. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "UploadImage.h"
#import "ImageEntry.h"

@interface DataManager : NSObject

// "public" arrays to be accessed by the user
@property (nonatomic, readonly, weak) NSArray *imageEntries;

// CoreData store
+ (DataManager *)sharedStore;

- (BOOL)save;

- (ImageEntry *)createImageEntryFromUploadImage:(UploadImage *)image;
- (UploadImage *)createUploadImage;
- (void)removeImageEntry:(UploadImage *)removeImage;

@end
