/*
 * Copyright 2010-2015 Amazon.com, Inc. or its affiliates. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License.
 * A copy of the License is located at
 *
 *  http://aws.amazon.com/apache2.0
 *
 * or in the "license" file accompanying this file. This file is distributed
 * on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied. See the License for the specific language governing
 * permissions and limitations under the License.
 */

#import "Constants.h"

#pragma mark - AWS Constants

AWSRegionType const CognitoRegionType = AWSRegionUSEast1; // e.g. AWSRegionUSEast1
AWSRegionType const DefaultServiceRegionType = AWSRegionUSWest2; // e.g. AWSRegionUSEast1
NSString *const CognitoIdentityPoolId = @"us-east-1:1f6419dc-e4cf-4db8-be4a-fd8b1a76da40";
NSString *const S3BucketName = @"cp130week6";

#pragma mark - ImageEntry Constants

NSString *const cognitoKey = @"USEastCognito";
NSString *const AWSDatasetKey = @"myDataset2";
NSString *const studentIDKey = @"jandros";