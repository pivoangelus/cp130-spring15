//
//  DataManager.m
//  hmwk05
//
//  Created by Justin Andros on 5/26/15.
//  Copyright (c) 2015 Justin Andros. All rights reserved.
//

#import "DataManager.h"
#import "Constants.h"
#import "ImageEntry.h"
#import <AWSDynamoDB/AWSDynamoDB.h>
#import "AWSDynamoDBObjectMapper.h"

@interface DataManager ()

@property (nonatomic, strong) NSMutableArray *pImageEntries;
@property (nonatomic, strong) NSManagedObjectModel *model;
@property (nonatomic, strong) NSManagedObjectContext *context;

@end

@implementation DataManager

- (instancetype)init
{
    self = [super init];
    
    if (!self)
        return self;
    
    self.model = [NSManagedObjectModel mergedModelFromBundles:nil];
    NSPersistentStoreCoordinator *coordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:self.model];
    
    NSArray *documentDirs = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *saveDir = [documentDirs firstObject];
    NSString *path = [saveDir stringByAppendingPathComponent:@"hmwk06Model.sqlite"];
    NSURL *storeURL = [NSURL fileURLWithPath:path];
    
    NSDictionary *options = @{ NSSQLitePragmasOption: @{@"journal_mode" : @" DELETE"}};
    NSError *error;
    
    if (![coordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:options error:&error])
        [NSException raise:@"Failed to open" format:@"%@", [error localizedDescription]];
    
    self.context = [[NSManagedObjectContext alloc] init];
    self.context.persistentStoreCoordinator = coordinator;
    
    // once the db is connected load in the aws db
    [self load];
    
    return self;
}

// create one store to be used throughout the application
+ (DataManager *)sharedStore
{
    static dispatch_once_t pred;
    static DataManager *sharedStore;
    
    dispatch_once(&pred, ^{
        sharedStore = [[self alloc] init];
    });
    
    return sharedStore;
}

- (NSArray *)imageEntries
{
    return [self.pImageEntries copy];   // give the user a copy to read
}

- (BOOL)save;
{
    NSError *error = nil;
    BOOL saved = [self.context save:&error];
    
    if (!saved)
        NSLog(@"save unsuccessful: %@", [error localizedDescription]);
    else
        NSLog(@"save successful");
    
    return saved;
}

- (ImageEntry *)createImageEntryFromUploadImage:(UploadImage *)uploadImage;
{
    ImageEntry *ie = [[ImageEntry alloc] init];
    ie.ID = uploadImage.imageID;
    ie.uploadName = uploadImage.uploadName;
    ie.studentID = uploadImage.studentID;
    return ie;
}

- (UploadImage *)createUploadImage;
{
    UploadImage *image = [NSEntityDescription insertNewObjectForEntityForName:@"UploadImage" inManagedObjectContext:self.context];
    [self.pImageEntries addObject:image];
    return image;
}

- (void)removeImageEntry:(UploadImage *)removeImage;
{
    [self.context deleteObject:removeImage];
    [self.pImageEntries removeObjectIdenticalTo:removeImage];
    [self save];
}

- (void)load
{
    // delete anything in the db
    NSFetchRequest *itemRequest = [[NSFetchRequest alloc] init];
    itemRequest.entity = [NSEntityDescription entityForName:@"UploadImage" inManagedObjectContext:self.context];
    
    NSError *error = nil;
    NSArray *itemResult = [self.context executeFetchRequest:itemRequest error:&error];
    
    for (NSManagedObject *mo in itemResult)
        [self.context deleteObject:mo];
    
    // start fresh
    self.pImageEntries = [NSMutableArray array];
    
    // connect to dynamoDB and search just for jandros (studentID)
    AWSDynamoDBObjectMapper *dynamoDBObjectMapper = [AWSDynamoDBObjectMapper defaultDynamoDBObjectMapper];
    AWSDynamoDBScanExpression *scanExpression = [AWSDynamoDBScanExpression new];
    AWSDynamoDBCondition *condition = [AWSDynamoDBCondition new];
    AWSDynamoDBAttributeValue *attribute = [AWSDynamoDBAttributeValue new];
    attribute.S = studentIDKey;
    condition.attributeValueList = @[attribute];
    condition.comparisonOperator = AWSDynamoDBComparisonOperatorEQ;
    scanExpression.scanFilter = @{@"studentID": condition};
    
    [[dynamoDBObjectMapper scan:[ImageEntry class] expression:scanExpression] continueWithBlock:^id(BFTask *task) {
        if (task.error)
            NSLog(@"Load failed. Error: [%@]", task.error);
        
        if (task.exception)
            NSLog(@"Load failed. Exception: [%@]", task.exception);
        
        // load in the data from amazon
        if (task.result)
        {
            AWSDynamoDBPaginatedOutput *paginatedOutput = task.result;
            for (ImageEntry *i in paginatedOutput.items)
            {
                NSLog(@"Found: %@", i.uploadName);
                UploadImage *image = [[DataManager sharedStore] createUploadImage];
                image.uploadName = i.uploadName;
                image.imageID = i.ID;
                image.studentID = i.studentID;
            }
        }
        return nil;
    }];

    NSLog(@"hmwk06Model.sqlite loaded");
}

@end
