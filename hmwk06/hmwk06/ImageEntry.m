//
//  ImageEntry.m
//  hmwk06
//
//  Created by Justin Andros on 6/5/15.
//  Copyright (c) 2015 Justin Andros. All rights reserved.
//

#import "ImageEntry.h"

@implementation ImageEntry

+ (NSString *)dynamoDBTableName
{
    return @"Class";
}

+ (NSString *)hashKeyAttribute
{
    return @"ID";
}

@end
