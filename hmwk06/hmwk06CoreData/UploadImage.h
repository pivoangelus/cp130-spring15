//
//  UploadImage.h
//  
//
//  Created by Justin Andros on 6/5/15.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface UploadImage : NSManagedObject

@property (nonatomic, retain) NSString * uploadName;
@property (nonatomic, retain) NSString * imageID;
@property (nonatomic, retain) NSString * studentID;

@end
