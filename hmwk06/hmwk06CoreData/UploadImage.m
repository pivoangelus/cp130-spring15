//
//  UploadImage.m
//  
//
//  Created by Justin Andros on 6/5/15.
//
//

#import "UploadImage.h"


@implementation UploadImage

@dynamic uploadName;
@dynamic imageID;
@dynamic studentID;

@end
