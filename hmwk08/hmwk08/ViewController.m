//
//  ViewController.m
//  hmwk08
//
//  Created by Justin Andros on 6/22/15.
//  Copyright (c) 2015 Justin Andros. All rights reserved.
//

#import "ViewController.h"

#pragma mark - IAP notifications keys
#define kInAppPurchaseManagerProductsFetchedNotification @"kInAppPurchaseManagerProductsFetchedNotification"
#define kInAppPurchaseManagerTransactionFailedNotification @"kInAppPurchaseManagerTransactionFailedNotification"
#define kInAppPurchaseManagerTransactionSucceededNotification @"kInAppPurchaseManagerTransactionSucceededNotification"

#pragma mark - IAP purchase keys
#define kInAppPurchaseProductPro @"edu.washington.jandros.hmwk08.proUpgrade"
#define kInAppPurchaseProductPoints @"edu.washington.jandros.hmwk08.points"
#define kProPurchased @"proPurchased"
#define kPointsPurchased @"pointsPurchased"
#define kTotalPoints @"totalPoints"

typedef NS_ENUM(NSUInteger, PurchaseTag)
{
    PurchasePoints = 1,
    PurchasePro = 2
};

@interface ViewController ()
{
    SKProduct *buyPro;
    SKProduct *buyPoints;
    SKProductsRequest *productsRequest;
    NSString *availablePurchases;
}

@property (weak, nonatomic) IBOutlet UILabel *pointsStatusLabel;
@property (weak, nonatomic) IBOutlet UILabel *proStatusLabel;
@property (weak, nonatomic) IBOutlet UITextView *purchasesTextView;
@property (weak, nonatomic) IBOutlet UIButton *pointsOutlet;
@property (weak, nonatomic) IBOutlet UIButton *proOutlet;

@end

@implementation ViewController

- (void)viewDidLoad
{
    // setup the view
    [super viewDidLoad];
    [self resetNSUserDefaults];
    [self updatePointsStatus];
    [self updateProStatus];
    
    // make sure to disable the buttons. force the user to start the store in this scenario.
    self.pointsOutlet.enabled = NO;
    self.proOutlet.enabled = NO;
}

// start clean
- (void)resetNSUserDefaults;
{
    NSString *domainName = [[NSBundle mainBundle] bundleIdentifier];
    [[NSUserDefaults standardUserDefaults] removePersistentDomainForName:domainName];
}

- (IBAction)purchaseTapped:(id)sender
{
    // see what the user purchased
    switch ([sender tag])
    {
        case PurchasePoints:
            [self purchaseProduct:buyPoints];
            break;
        case PurchasePro:
            [self purchaseProduct:buyPro];
            break;
    }
}

- (IBAction)openStoreTapped:(id)sender
{
    // restarts any purchases if they were interrupted last time the app was open
    [[SKPaymentQueue defaultQueue] addTransactionObserver:self];
    
    // get the product description
    [self requestProductsData];
}

- (void)requestProductsData
{
    NSSet *productIdentifiers = [NSSet setWithObjects:kInAppPurchaseProductPoints, kInAppPurchaseProductPro, nil];
    productsRequest = [[SKProductsRequest alloc] initWithProductIdentifiers:productIdentifiers];
    productsRequest.delegate = self;
    [productsRequest start];
    self.pointsOutlet.enabled = YES;
    [self updateProStatus];
}

// call this before making a purchase
- (BOOL)canMakePurchases
{
    return [SKPaymentQueue canMakePayments];
}

// buying!
- (void)purchaseProduct:(SKProduct *)buyProduct
{
    SKPayment *payment = [SKPayment paymentWithProduct:buyProduct];
    [[SKPaymentQueue defaultQueue] addPayment:payment];
}

// enabling features
- (void)provideContent:(NSString *)productId
{
    if ([productId isEqualToString:kInAppPurchaseProductPoints])
    {
        NSInteger points = [[NSUserDefaults standardUserDefaults] integerForKey:kTotalPoints];
        // enable!
        [[NSUserDefaults standardUserDefaults] setInteger:(points + 10) forKey:kTotalPoints];
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:kPointsPurchased];
        [[NSUserDefaults standardUserDefaults] synchronize];
        [self updatePointsStatus];
    }
    
    if ([productId isEqualToString:kInAppPurchaseProductPro])
    {
        // enable!
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:kProPurchased];
        [[NSUserDefaults standardUserDefaults] synchronize];
        [self updateProStatus];
    }
}

// removes the transaction from the queue and posts a notification with the transaction result
- (void)finishTransaction:(SKPaymentTransaction *)transaction wasSuccessful:(BOOL)wasSuccessful
{
    // remove the transaction from the payment queue
    [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
    
    NSDictionary *userInfo = [NSDictionary dictionaryWithObjectsAndKeys:transaction, @"transaction", nil];
    
    if (wasSuccessful)
    {
        // send out a notification that we've finished the transaction
        [[NSNotificationCenter defaultCenter] postNotificationName:kInAppPurchaseManagerTransactionSucceededNotification object:self userInfo:userInfo];
    }
    else
    {
        // send out a notification for the failed tranaction
        [[NSNotificationCenter defaultCenter] postNotificationName:kInAppPurchaseManagerTransactionFailedNotification object:self userInfo:userInfo];
    }
}

// called when the transaction was successful
- (void)completeTransaction:(SKPaymentTransaction *)transaction
{
    [self provideContent:transaction.payment.productIdentifier];
    [self finishTransaction:transaction wasSuccessful:YES];
    
    NSLog(@"Product purchased: %@", transaction.payment.productIdentifier);
}

// called when a transaction has been restored and successfully completed
- (void)restoreTransaction:(SKPaymentTransaction *)transaction
{
    [self provideContent:transaction.originalTransaction.payment.productIdentifier];
    [self finishTransaction:transaction wasSuccessful:YES];
    
    NSLog(@"Product restored: %@", transaction.payment.productIdentifier);
}

// called when a transaction has failed
- (void)failedTransaction:(SKPaymentTransaction *)transaction
{
    if (transaction.error.code != SKErrorPaymentCancelled)
    {
        [self finishTransaction:transaction wasSuccessful:NO];
        NSLog(@"error");
    }
    else
    {
        // user canceled
        [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
        NSLog(@"Buy canceled");
    }
}

- (void)updateProStatus
{
    NSString *proStatus = @"Buy Pro Today!";
    self.proOutlet.enabled = YES;
    
    if ([[NSUserDefaults standardUserDefaults] boolForKey:kProPurchased])
    {
        proStatus = @"Welcome to Pro!";
        self.proOutlet.enabled = NO;
    }
    
    self.proStatusLabel.text = proStatus;
}

- (void)updatePointsStatus
{
    NSInteger points = [[NSUserDefaults standardUserDefaults] integerForKey:kTotalPoints];
    NSString *pointsStatus = [NSString stringWithFormat:@"You currently have %ld points.", (long)points];
    
    self.pointsStatusLabel.text = pointsStatus;
}

#pragma mark - <SKPaymentTransactionObserver>

// called when the tranaction status is updated
- (void)paymentQueue:(SKPaymentQueue *)queue updatedTransactions:(NSArray *)transactions
{
    for (SKPaymentTransaction *transaction in transactions)
    {
        switch (transaction.transactionState)
        {
            case SKPaymentTransactionStatePurchased:
                [self completeTransaction:transaction];
                break;
            case SKPaymentTransactionStateFailed:
                [self failedTransaction:transaction];
                break;
            case SKPaymentTransactionStateRestored:
                [self restoreTransaction:transaction];
                break;
            default:
                break;
        }
    }
}

#pragma mark - <SKProductsRequestDelegate>

- (void)productsRequest:(SKProductsRequest *)request didReceiveResponse:(SKProductsResponse *)response
{
    availablePurchases = @"";
    NSArray *products = response.products;
    if (products)
    {
        for (SKProduct *p in products)
        {
            if ([p.productIdentifier isEqualToString:kInAppPurchaseProductPoints])
                buyPoints = p;
            
            if ([p.productIdentifier isEqualToString:kInAppPurchaseProductPro])
                buyPro = p;
            
            NSLog(@"Product title: %@", p.localizedTitle);
            NSLog(@"Product description: %@", p.localizedDescription);
            NSLog(@"Product price: %@", p.price);
            NSLog(@"Product id: %@", p.productIdentifier);
            
            availablePurchases = [availablePurchases stringByAppendingFormat:@"Title: %@\nDescription: %@\nPrice: %@\nID: %@\n\n", p.localizedTitle, p.localizedDescription, p.price, p.productIdentifier];
        }
    }
    
    for (NSString *invalidProductId in response.invalidProductIdentifiers)
    {
        NSLog(@"Invalid product id: %@", invalidProductId);
    }
    
    [[NSNotificationCenter defaultCenter] postNotificationName:kInAppPurchaseManagerProductsFetchedNotification object:self userInfo:nil];
    self.purchasesTextView.text = availablePurchases;
}

@end