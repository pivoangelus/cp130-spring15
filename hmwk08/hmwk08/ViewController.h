//
//  ViewController.h
//  hmwk08
//
//  Created by Justin Andros on 6/22/15.
//  Copyright (c) 2015 Justin Andros. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <StoreKit/StoreKit.h>

@interface ViewController : UIViewController
<
    SKProductsRequestDelegate,
    SKPaymentTransactionObserver
>

@end